var canvas = document.getElementById("c1");
var ctx = canvas.getContext("2d");

function sing_axis (text, x, y) {
  ctx.font = '7pt Roboto';
  ctx.fillStyle = '#abc';
  ctx.fillText(text, x, y);
}

function sing_grid () {
  for (let i = 0; i < 10; i++) {
    sing_axis(50*i, 50*i + 1, 8 );
  }

  for (let i = 0; i < 7; i++) {
    sing_axis(50*i, 0, (50*i - 2));
  }
}

function show_coords(pos) {
  let [x, y, w, h] = [canvas.width - 62, canvas.height - 16, 58, 12];

  ctx.fillStyle = '#abc';
  ctx.fillRect(x, y, w, h);
  ctx.font = '7pt Roboto';
  ctx.fillStyle = '#222';
  ctx.fillText('x: ' + pos.x, x + 3, y + 9);
  ctx.fillText('y: ' + pos.y, x + 30, y + 9);
}

function get_mouse_pos (canvas, event)  {
  let rect = canvas.getBoundingClientRect();

  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top
  };
}

// canvas.addEventListener(
//   'mousemove',
//   function (event) {
//     let mouse_pos = get_mouse_pos(canvas, event);
//     // let msg = "x: " + mouse_pos.x + ", y: " + mouse_pos.y;
//     show_coords(mouse_pos);
//   },
//   false
// );

canvas.onmousemove = function (e) {
  let mouse_pos = get_mouse_pos(canvas, e);

  show_coords(mouse_pos);
}

sing_grid();
