Get started quickly
Creating a README or a .gitignore is a quick and easy way to get something into your repository.

Create a README
Create a .gitignore
Get your local Git repository on Bitbucket

Step 1: Switch to your repository's directory
```
cd /path/to/your/repo
```

Step 2: Connect your existing repository to Bitbucket
```
git remote add origin git@bitbucket.org:bagur/canvas.git
git push -u origin master
```

Need more information? Learn more